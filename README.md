# ics-ans-role-mssql

Ansible role to install mssql.

Options:
1. Evaluation (free, no production use rights, 180-day limit)
2. Developer (free, no production use rights)
3. Express (free)
4. Web (PAID)
5. Standard (PAID)
6. Enterprise (PAID)
7. Enterprise Core (PAID)

## Role Variables

```yaml
mssql_container_name: "container_name"
mssql_databases: [] #list of databases to create and can be blank
mssql_server_image_name: "mcr.microsoft.com/mssql/server"
mssql_server_tag: "2019-latest"
# Recent images require min 8 chars (at least 1 of each; lowercase, uppercase, numeric and symbol)
mssql_server_password: "Replaceme10#Please"
mssql_server_port: "1433"
mssql_server_edition: # can be Developer, Express, Standard, Enterprise, EnterpriseCore
mssql_server_path: /opt/mssql
mssql_agent_enabled: "true"
mssql_server_mem_limit: "2048"
mssql_server_timezone: "Europe/Stockholm"
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-mssql
```

## License

BSD 2-clause
