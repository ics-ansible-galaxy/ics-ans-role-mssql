import os
import yaml
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


ans_def_var = open("../../defaults/main.yml")
parse_yaml = yaml.load(ans_def_var, Loader=yaml.FullLoader)
dbname = parse_yaml["mssql_databases"]
pword = parse_yaml["mssql_server_password"]
contname = parse_yaml["mssql_container_name"]


def test_is_docker_installed(host):
    package_docker = host.package('docker-ce')
    assert package_docker.is_installed
    docker_service = host.service('docker.service')
    assert docker_service.is_enabled
    assert docker_service.is_running


def test_container(host):
    with host.sudo():
        mssql = host.docker(contname)
        assert mssql.is_running


def test_database_exists(host):
    sqlcmd = "/opt/mssql-tools/bin/sqlcmd"
    cmd = host.run(sqlcmd + " -S localhost -U SA -P " + pword + " -I -Q 'SELECT name FROM master.sys.databases'")
    if dbname:
        for item in dbname:
            assert item in cmd.stdout
    else:
        assert True
